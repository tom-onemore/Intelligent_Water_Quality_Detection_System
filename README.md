# 基于OpenHarmony的智能水质检测系统

## 应用背景

&emsp;&emsp; 当今世界，全球性缺水正在逐年加重，据世界气象组织报告，到2050年将有50亿人面临缺水问题。水是生命之源，世界上有数以万计的人因饮用了受到污染的水而生病，甚至死亡。我们这个项目基于OpenHarmony设计了一款智能水质检测系统，将对水体的浑浊度，pH值等进行分析，如果水质不达标会及时报警，让更多的人喝上干净的水。绿水青山就是金山银山，保护好水资源就是保护好我们的家园。良好的水体有助于改善气候，同时也会让地球上的生物有更加良好的生存环境。人与自然，命运与共，让我们一起构建美好的地球家园！

## 响应联合国17项可持续发展目标

1. 良好的健康与福祉
2. 清洁饮水和卫生设施
3. 气候行动
4. 水下生物
5. 陆地生物

## 实现功能

### 水体浑浊度检测

&emsp;&emsp; 水体浑浊度检测我们使用的是AZDM01浊度传感器，根据红外信号在不同浑浊度水体中的返回信号不同，返回相应的ADC数值，根据TDS解算公式，计算出相应的浑浊度。

### 数据可视化

#### 串口显示

&emsp;&emsp; 利用串口工具，实时查看相应TDS值，及ADC值。

#### 云端显示

&emsp;&emsp; 利用OneNET - 中国移动物联网开放平台，使用MQTT接入，实时查看数据流，以折线图形式展示，直观清晰，以及设备上线、离线提醒。

## 系统框架

![系统框架](picture/%E7%B3%BB%E7%BB%9F%E6%A1%86%E6%9E%B6.png)

## 硬件介绍

### BearPi-HM_Nano

#### 实物图片

![实物图片（BearPi-HM_Nano）](picture/%E5%AE%9E%E7%89%A9%E5%9B%BE%E7%89%87%EF%BC%88BearPi-HM_Nano%EF%BC%89.jpg)

#### 产品参数

![产品参数（BearPi-HM_Nano）](picture/%E4%BA%A7%E5%93%81%E5%8F%82%E6%95%B0%EF%BC%88BearPi-HM_Nano%EF%BC%89.jpg)

### AZDM01

#### 实物图片

![实物图片（AZDM01）](picture/%E5%AE%9E%E7%89%A9%E5%9B%BE%E7%89%87%EF%BC%88AZDM01%EF%BC%89.jpg)

#### 产品参数

![产品参数（AZDM01）](picture/%E4%BA%A7%E5%93%81%E5%8F%82%E6%95%B0%EF%BC%88AZDM01%EF%BC%89.jpg)

### 浊度模块

#### 实物图片

![实物图片（浊度模块）](picture/%E5%AE%9E%E7%89%A9%E5%9B%BE%E7%89%87%EF%BC%88%E6%B5%8A%E5%BA%A6%E6%A8%A1%E5%9D%97%EF%BC%89.jpg)

#### 产品参数

![产品参数（浊度模块）](picture/%E4%BA%A7%E5%93%81%E5%8F%82%E6%95%B0%EF%BC%88%E6%B5%8A%E5%BA%A6%E6%A8%A1%E5%9D%97%EF%BC%89.jpg)

## 作品完整图片

### 主视图

![主视图](picture/%E4%B8%BB%E8%A7%86%E5%9B%BE.jpg)

### 俯视图

![俯视图](picture/%E4%BF%AF%E8%A7%86%E5%9B%BE.jpg)

### 串口显示

![串口显示](picture/%E4%B8%B2%E5%8F%A3%E6%98%BE%E7%A4%BA.jpg)

### 云端显示

![云端显示](picture/%E4%BA%91%E7%AB%AF%E6%98%BE%E7%A4%BA.png)

### 上线离线提醒

![上线离线提醒](picture/%E4%B8%8A%E7%BA%BF%E7%A6%BB%E7%BA%BF%E6%8F%90%E9%86%92.png)

## 视频演示效果

[基于OpenHarmony的智能水质检测系统](https://www.bilibili.com/video/BV1AV4y1s7Xm?share_source=copy_web&vd_source=71cfadc7d8d3aeefa7b9c52fcfae11a1)

## 代码验证

- 将 onenet_test 文件夹放入 applications\sample\wifi-iot\app 目录下
- 将 applications\sample\wifi-iot\app 下的 BUILD.gn 替换为本文件夹中的 BUILD.gn
- 需要用到第三方库 [harmony_mqtt](https://gitee.com/lianzhian/harmony_mqtt) 和 [harmony_onenet](https://gitee.com/lianzhian/harmony_onenet) ，请将 clone 后的文件夹放入 applications\third_party 目录下，将原有的 paho_mqtt 文件夹删除
- 可以正常编译，烧录
- 作品相关图片请见 picture 文件夹
- 本项目基于 OpenHarmony 3.1 Release 版本

## 项目展望

&emsp;&emsp; 在下一阶段，将实现水体pH检测，以及开发基于DAYU200的智能水质检测系统APP，实现云端同步。

## 参考链接

[从零开始搭建OpenHarmony开发环境](https://www.bilibili.com/video/BV16R4y157xv?share_source=copy_web&vd_source=71cfadc7d8d3aeefa7b9c52fcfae11a1)

[小熊派开源社区 / BearPi-HM_Nano](https://gitee.com/bearpi/bearpi-hm_nano)

[OpenHarmony-SIG / knowledge_demo_smart_home](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)